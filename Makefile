.DEFAULT_GOAL := build
.PHONY:	build clean

build:
	gcc -fPIC -c pkg/c/hello.c -o pkg/c/hello.o
	gcc -shared -o pkg/c/libhello.so pkg/c/hello.o
	go build .

clean:
	rm -rf pkg/c/*.o pkg/c/*.so
