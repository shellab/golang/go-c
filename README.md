# Go with C

## build c static library

```bash=
gcc -c pkg/c/hello.c -o pkg/c/hello.o
ar rcs pkg/c/libhello.a pkg/c/hello.o
```

## build c shared library

```bash=
gcc -fPIC -c pkg/c/hello.c -o pkg/c/hello.o
gcc -shared -o pkg/c/libhello.so pkg/c/hello.o
```

## build go

```bash=
go build .
```
