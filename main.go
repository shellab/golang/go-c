package main

/*
#cgo CFLAGS: -I./pkg/c
#cgo LDFLAGS: -L./pkg/c -lhello

#include <stdio.h>
#include <stdlib.h>
#include "hello.h"
*/
import "C"
import "unsafe"

// import "unsafe"

func main() {
	// let's call it
	cs := C.CString("Hello from stdio")
	C.hello(cs)
	C.free(unsafe.Pointer(cs))
}
